## 0.2.0 (2022-03-14)

### Fix

- **layouts/partials/head.html**: removed Not Serif JP
- **layouts/partials/head.html**: Googleフォントのウェイトを微調整した
- **main.css**: ブログタイトルやページタイトルのフォントサイズをおとなしくした

### Feat

- **static/css/main.css**: Noto Sans JP / Noto Serif JP を追加した
- **layouts/partials/head.html**: 日本語Googleフォントを追加した

## 0.1.0 (2022-02-22)

### Feat

- サイト設定でshare_imgを使ったOGPを設定できるようにした
- サイト設定でshare_imgを使ったOGPを設定できるようにした

## 0.0.1 (2022-02-22)

### Fix

- **.cz.toml**: バージョンを0.0.0からはじめることにした
- **.cz.toml**: czを追加した
- **post_preview.html**: 一覧に表示するタイトルを LinkTitle に変更した
- **head.html**: トップページとその他のページでtitleタグの出力が切り替わるようにした
- **head.html**: トップページとその他のページでtitleタグの出力が切り替わるようにした
- **main.css**: removed font-family:italic
- **main.css**: removed font-family:italic
- **ja.yaml**: lastModifiedの日本語訳を修正した
- **ja.yaml**: pageNotFound の日本語訳を修正した
- **ja.yaml**: show の日本語訳を変更した
- **ja.yaml**: seeAlsoの日本語訳を修正した
- **ja.yaml**: words の日本語訳を修正した
- **ja.yaml**: poweredByを英語のままにした
- **config.toml**: Authorを変更した
- **config.toml**: bigimg を有効にした
- **config.toml**: dateFormatを変更した
- **config.toml**: Paramsを変更した
- **config.toml**: デフォルト言語を日本語に変更した
- support dates in Hugo 0.76 (but not earlier)
- support Hugo 0.76 (but not earlier)
